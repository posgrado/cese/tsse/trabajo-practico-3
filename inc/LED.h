/****************************************************************************
*	Avoid multiple inclusion - begin										*
*****************************************************************************/
#ifndef _LED_H_
#define _LED_H_

/**
 * @file LED.h
 * @author David Broin <davidmbroin@gmail.com>
 * @date 2020/08/18
 * @brief File containing LED functions
 * @copyright All rights reserved.
 * @copyright @ref [MIT LICENSE](LICENSE.txt)
 */

/****************************************************************************
*	Inclusions of public function dependencies								*
*****************************************************************************/

#include <stdint.h>
#include <stdbool.h>

/****************************************************************************
*	C++ - begin																*
*****************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/****************************************************************************
*	Definition macros of public constants									*
*****************************************************************************/

/****************************************************************************
*	Public function-like macros												*
*****************************************************************************/

/****************************************************************************
*	Definitions of public data types										*
*****************************************************************************/

/****************************************************************************
*	Prototypes (declarations) of public functions							*
*****************************************************************************/

/**
 * @brief Crea los leds
 * @param direccion del puerto donde estan los leds
 */
void LedsCreate(int* direccion);

/**
 * @brief Enciende un LED
 * @param numero de LED (de 1 a 16)
 */
void LedsTurnOn(uint8_t led);

/**
 * @brief Apaga un LED
 * @param numero de LED (de 1 a 16)
 */
void LedsTurnOff(uint8_t led);

/**
 * @brief Lee el estado del led
 * @param numero de LED (de 1 a 16)
 * @return estado del LED 1 o 0
 */
bool LedState(uint8_t led);

/**
 * @brief Enciende todos los LEDs
 */
void LedsTurnAllOn();

/**
 * @brief Apaga todos los LEDs
 */
void LedsTurnAllOff();

/****************************************************************************
*	Prototypes (declarations) of public interrupt functions					*
*****************************************************************************/

/****************************************************************************
*	C++ - end																*
*****************************************************************************/

#ifdef __cplusplus
}
#endif

/****************************************************************************
*	Avoid multiple inclusion - end											*
*****************************************************************************/

#endif /* _LED_H_ */

