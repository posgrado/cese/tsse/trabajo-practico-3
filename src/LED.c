/****************************************************************************
 * @file LED.c
 * @author David Broin <davidmbroin@gmail.com>
 * @date 2020/08/18
 * @brief Codigo fuente para manejo de LEDs
 * @copyright All rights reserved.
 * @copyright @ref [MIT LICENSE](LICENSE.txt)
 ****************************************************************************/

/****************************************************************************
 *	Inclusions of public function dependencies                              *
 *****************************************************************************/

#include "LED.h"

/****************************************************************************
 *	Definition macros of private constants                                  *
 *****************************************************************************/

/****************************************************************************
 *	Definitions of private global variables                                  *
 *****************************************************************************/

static int * puerto;

/****************************************************************************
 *	Prototypes (declarations) of private functions                          *
 *****************************************************************************/

/****************************************************************************
 *	Implementations of public functions                                     *
 *****************************************************************************/

void LedsCreate(int* direccion) {
	puerto = direccion;
	*puerto = 0x0000;
}

void LedsTurnOn(uint8_t led) {
	*puerto |= 1 << (led - 1);
}

void LedsTurnOff(uint8_t led) {
	*puerto &= ~(1 << (led - 1));
}

bool LedState(uint8_t led){
	return *puerto & (1 << (led - 1));
}

void LedsTurnAllOn(){
	*puerto |= 0xFFFF;
}

void LedsTurnAllOff(){
	*puerto &= 0x0000;
}
