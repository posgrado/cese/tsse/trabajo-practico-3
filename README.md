# Trabajo Practico 3 TDD
Completar el trabajo presentado en clase para desarrollar un controlador
 de led utilizando la técnica de TDD. Se deben completar las siguientes
  pruebas:

- Después de la inicialización todos los LEDs
deben quedar apagados.
- Se puede prender un LED individual.
- Se puede apagar un LED individual.
- Se pueden prender y apagar múltiples LED’s.
- Se pueden prender todos los LEDs de una vez.
- Se pueden apagar todos los LEDs de una vez.
- Se puede consultar el estado de un LED.
- Revisar limites de los parametros.
- Revisar parámetros fuera de los limites.

## Getting Started
### Prerequisites

## Contact

If you want to contact me you can reach me at davidmbroin@gmail.com.

## License

This project uses the following license: [MIT](LICENSE.txt).
