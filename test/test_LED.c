/**
 * Despues de la inicializacion todos los LEDs
 * deben quedar apagados.
 * Se puede prender un LED individual.
 * Se puede apagar un LED individual.
 * Se pueden prender y apagar multiples LEDs.
 * Se pueden prender todos los LEDs de una vez.
 * Se pueden apagar todos los LEDs de una vez.
 * Se puede consultar el estado de un LED.
 * Revisar limites de los parametros.
 * Revisar parametros fuera de los limites.
 */

#include "unity.h"
#include "LED.h"
int virtuales = 0xFFFF;

void setUp(void) {
	LedsCreate(&virtuales);
}

void test_todos_los_leds_inician_apagados(void) {
	TEST_ASSERT_EQUAL(0x0000, virtuales);
}

void test_encender_un_led(void) {
	LedsTurnOn(1);
	TEST_ASSERT_EQUAL(0x0001, virtuales);
}

void test_encender_un_led_y_apagar_un_led(void) {
	LedsTurnOn(1);
	LedsTurnOff(1);
	TEST_ASSERT_EQUAL(0x0000, virtuales);
}

void test_encender_y_apagar_varios_leds(void) {
	LedsTurnOn(3);
	LedsTurnOn(5);
	LedsTurnOff(3);
	TEST_ASSERT_EQUAL(0x0010, virtuales);
}

void test_Read_Leds_Port_State() {
	LedsTurnOn(8);
	LedsTurnOn(2);
	LedsTurnOn(14);
	TEST_ASSERT_EQUAL_HEX16(1, LedState(8));
	TEST_ASSERT_EQUAL_HEX16(1, LedState(2));
	TEST_ASSERT_EQUAL_HEX16(1, LedState(14));
	TEST_ASSERT_EQUAL_HEX16(0, LedState(6));
	TEST_ASSERT_EQUAL_HEX16(0, LedState(13));
}

void test_encender_todos_los_leds(void) {
	LedsTurnAllOn();
	TEST_ASSERT_EQUAL(0xFFFF, virtuales);
}

void test_apagar_todos_los_leds(void) {
	LedsTurnAllOn();
	LedsTurnAllOff();
	TEST_ASSERT_EQUAL_HEX16(0, virtuales);
}

void test_encender_extremos(void) {
	LedsTurnOn(1);
	LedsTurnOn(16);
	TEST_ASSERT_EQUAL(0x8001, virtuales);
}

void test_apagar_extremos(void) {
	LedsTurnAllOn();
	LedsTurnOff(1);
	LedsTurnOff(16);
	TEST_ASSERT_EQUAL_HEX16(0x7FFE, virtuales);
}

void test_encendido_fuera_rango(void) {
	LedsTurnOn(-1);
	LedsTurnOn(17);
	TEST_ASSERT_EQUAL_HEX16(0x0000, virtuales);
}

void test_apagado_fuera_rango(void) {
	LedsTurnAllOn();
	LedsTurnOff(-1);
	LedsTurnOff(17);
	TEST_ASSERT_EQUAL_HEX16(0xFFFF, virtuales);
}
